package ru.tsc.almukhametov.tm.command.union;

import ru.tsc.almukhametov.tm.command.AbstractProjectTaskCommand;
import ru.tsc.almukhametov.tm.constant.SystemDescriptionConst;
import ru.tsc.almukhametov.tm.constant.TerminalConst;
import ru.tsc.almukhametov.tm.exception.entity.TaskNotFoundException;
import ru.tsc.almukhametov.tm.model.Task;
import ru.tsc.almukhametov.tm.util.TerminalUtil;

import java.util.List;

public class ProjectFindAllTasksCommand extends AbstractProjectTaskCommand {

    @Override
    public String name() {
        return TerminalConst.TASK_LIST_BY_PROJECT;
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return SystemDescriptionConst.TASK_LIST_BY_PROJECT;
    }

    @Override
    public void execute() {
        System.out.println("[Task list by project]");
        System.out.println("Enter project id");
        final String projectId = TerminalUtil.nextLine();
        final List<Task> tasks = getProjectTaskService().findAllTaskByProjectId(projectId);
        if (tasks == null || tasks.isEmpty()) throw new TaskNotFoundException();
        System.out.println("Task list for project");
        int index = 1;
        for (Task task : tasks) {
            System.out.println(index + ". " + task.toString());
            index++;
        }
    }
}
