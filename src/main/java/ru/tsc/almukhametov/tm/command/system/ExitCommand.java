package ru.tsc.almukhametov.tm.command.system;

import ru.tsc.almukhametov.tm.command.AbstractCommand;
import ru.tsc.almukhametov.tm.constant.SystemDescriptionConst;
import ru.tsc.almukhametov.tm.constant.TerminalConst;

public class ExitCommand extends AbstractCommand {

    @Override
    public String name() {
        return TerminalConst.EXIT;
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return SystemDescriptionConst.EXIT;
    }

    @Override
    public void execute() {
        System.exit(0);
    }
}
