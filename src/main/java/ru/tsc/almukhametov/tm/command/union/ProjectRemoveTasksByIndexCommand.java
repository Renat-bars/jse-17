package ru.tsc.almukhametov.tm.command.union;

import ru.tsc.almukhametov.tm.command.AbstractProjectTaskCommand;
import ru.tsc.almukhametov.tm.constant.SystemDescriptionConst;
import ru.tsc.almukhametov.tm.constant.TerminalConst;
import ru.tsc.almukhametov.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.almukhametov.tm.model.Project;
import ru.tsc.almukhametov.tm.util.TerminalUtil;

public class ProjectRemoveTasksByIndexCommand extends AbstractProjectTaskCommand {

    @Override
    public String name() {
        return TerminalConst.PROJECT_REMOVE_BY_INDEX;
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return SystemDescriptionConst.PROJECT_REMOVE_BY_INDEX;
    }

    @Override
    public void execute() {
        System.out.println("Enter Index");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = getProjectTaskService().removeByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
    }
}
