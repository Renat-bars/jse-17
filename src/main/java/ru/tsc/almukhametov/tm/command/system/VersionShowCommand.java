package ru.tsc.almukhametov.tm.command.system;

import ru.tsc.almukhametov.tm.command.AbstractCommand;
import ru.tsc.almukhametov.tm.constant.ArgumentConst;
import ru.tsc.almukhametov.tm.constant.SystemDescriptionConst;
import ru.tsc.almukhametov.tm.constant.TerminalConst;

public class VersionShowCommand extends AbstractCommand {

    @Override
    public String name() {
        return TerminalConst.VERSION;
    }

    @Override
    public String arg() {
        return ArgumentConst.VERSION;
    }

    @Override
    public String description() {
        return SystemDescriptionConst.VERSION;
    }

    @Override
    public void execute() {
        System.out.println("[VERSION]");
        System.out.println("1.0.1");
    }
}
