package ru.tsc.almukhametov.tm.command.union;

import ru.tsc.almukhametov.tm.command.AbstractProjectTaskCommand;
import ru.tsc.almukhametov.tm.constant.SystemDescriptionConst;
import ru.tsc.almukhametov.tm.constant.TerminalConst;
import ru.tsc.almukhametov.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.almukhametov.tm.model.Project;
import ru.tsc.almukhametov.tm.util.TerminalUtil;

public class ProjectRemoveTasksByNameCommand extends AbstractProjectTaskCommand {

    @Override
    public String name() {
        return TerminalConst.PROJECT_REMOVE_BY_NAME;
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return SystemDescriptionConst.PROJECT_REMOVE_BY_NAME;
    }

    @Override
    public void execute() {
        System.out.println("Enter Name");
        final String name = TerminalUtil.nextLine();
        final Project project = getProjectTaskService().removeByName(name);
        if (project == null) throw new ProjectNotFoundException();
    }
}
