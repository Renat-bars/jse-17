package ru.tsc.almukhametov.tm.command.task;

import ru.tsc.almukhametov.tm.command.AbstractTaskCommand;
import ru.tsc.almukhametov.tm.constant.SystemDescriptionConst;
import ru.tsc.almukhametov.tm.constant.TerminalConst;
import ru.tsc.almukhametov.tm.enumerated.Status;
import ru.tsc.almukhametov.tm.exception.entity.TaskNotFoundException;
import ru.tsc.almukhametov.tm.model.Task;
import ru.tsc.almukhametov.tm.util.TerminalUtil;

import java.util.Arrays;

public class TaskChangeByNameCommand extends AbstractTaskCommand {
    @Override
    public String name() {
        return TerminalConst.TASK_CHANGE_STATUS_BY_NAME;
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return SystemDescriptionConst.TASK_CHANGE_STATUS_BY_NAME;
    }

    @Override
    public void execute() {
        System.out.println("Enter Name");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter Status");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.valueOf(statusValue);
        final Task task = getTaskService().changeTaskStatusByName(name, status);
        if (task == null) throw new TaskNotFoundException();
    }
}
