package ru.tsc.almukhametov.tm.service;

import ru.tsc.almukhametov.tm.api.repository.ICommandRepository;
import ru.tsc.almukhametov.tm.api.service.ICommandService;
import ru.tsc.almukhametov.tm.command.AbstractCommand;
import ru.tsc.almukhametov.tm.model.Command;

import java.util.Collection;

public class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public AbstractCommand getCommandByName(final String name) {
        if (name == null || name.isEmpty()) return null;
        return commandRepository.getCommandByName(name);
    }

    @Override
    public AbstractCommand getCommandByArg(final String arg) {
        if (arg == null || arg.isEmpty()) return null;
        return commandRepository.getCommandByArg(arg);
    }

    @Override
    public Collection<AbstractCommand> getCommands() { return commandRepository.getCommands(); }

    @Override
    public Collection<AbstractCommand> getArguments() { return commandRepository.getArguments(); }

    @Override
    public Collection<String> getListCommandName() { return commandRepository.getCommandNames(); }

    @Override
    public Collection<String> getListCommandArg() { return commandRepository.getCommandArg(); }

    @Override
    public void add(AbstractCommand command) {
        if (command == null) return;
        commandRepository.add(command);
    }
}
