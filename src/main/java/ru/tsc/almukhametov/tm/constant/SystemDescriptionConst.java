package ru.tsc.almukhametov.tm.constant;

public class SystemDescriptionConst {

    public static final String ABOUT = "display developer info.";
    public static final String HELP = "display list of commands";
    public static final String VERSION = "display program version.";
    public static final String INFO = "display system information.";
    public static final String COMMANDS = "display list of commands.";
    public static final String ARGUMENTS = "display list arguments.";
    public static final String PROJECT_LIST = "show project list.";
    public static final String PROJECT_CREATE = "create new project.";
    public static final String PROJECT_CLEAR = "remove all project.";
    public static final String PROJECT_SHOW_BY_ID = "show project by id.";
    public static final String PROJECT_SHOW_BY_INDEX = "show project by index.";
    public static final String PROJECT_SHOW_BY_NAME = "show project by name.";
    public static final String PROJECT_REMOVE_BY_ID = "remove project by id.";
    public static final String PROJECT_REMOVE_BY_INDEX = "remove project by index.";
    public static final String PROJECT_REMOVE_BY_NAME = "remove project by name.";
    public static final String PROJECT_UPDATE_BY_ID = "update project by id.";
    public static final String PROJECT_UPDATE_BY_INDEX = "update project by index.";
    public static final String PROJECT_START_BY_ID = "start project by id.";
    public static final String PROJECT_START_BY_INDEX = "start project by index.";
    public static final String PROJECT_START_BY_NAME = "start project by name.";
    public static final String PROJECT_FINISH_BY_ID = "finish project by id.";
    public static final String PROJECT_FINISH_BY_INDEX = "finish project by index.";
    public static final String PROJECT_FINISH_BY_NAME = "finish project by name.";
    public static final String PROJECT_CHANGE_STATUS_BY_ID = "change project status  by id.";
    public static final String PROJECT_CHANGE_STATUS_BY_INDEX = "change project status by index.";
    public static final String PROJECT_CHANGE_STATUS_BY_NAME = "change project status by name.";
    public static final String TASK_LIST = "show task list.";
    public static final String TASK_CREATE = "create new task.";
    public static final String TASK_CLEAR = "remove all task.";
    public static final String TASK_SHOW_BY_ID = "show task by id.";
    public static final String TASK_SHOW_BY_INDEX = "show task by index.";
    public static final String TASK_SHOW_BY_NAME = "show task by name.";
    public static final String TASK_REMOVE_BY_ID = "remove task by id.";
    public static final String TASK_REMOVE_BY_INDEX = "remove task by index.";
    public static final String TASK_REMOVE_BY_NAME = "remove task by name.";
    public static final String TASK_UPDATE_BY_ID = "update task by id.";
    public static final String TASK_UPDATE_BY_INDEX = "update task by index.";
    public static final String TASK_START_BY_ID = "start task by id.";
    public static final String TASK_START_BY_INDEX = "start task by index.";
    public static final String TASK_START_BY_NAME = "start task by name.";
    public static final String TASK_FINISH_BY_ID = "finish task by id.";
    public static final String TASK_FINISH_BY_INDEX = "finish task by index.";
    public static final String TASK_FINISH_BY_NAME = "finish task by name";
    public static final String TASK_CHANGE_STATUS_BY_ID = "change task status  by id.";
    public static final String TASK_CHANGE_STATUS_BY_INDEX = "change task status by index.";
    public static final String TASK_CHANGE_STATUS_BY_NAME = "change task status by name.";
    public static final String TASK_LIST_BY_PROJECT = "show task list by project.";
    public static final String TASK_BIND_TO_PROJECT = "bind task to project.";
    public static final String TASK_UNBIND_FROM_PROJECT = "unbind task from project.";
    public static final String EXIT = "close application.";

}
