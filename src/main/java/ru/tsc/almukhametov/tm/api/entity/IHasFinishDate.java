package ru.tsc.almukhametov.tm.api.entity;

import java.util.Date;

public interface IHasFinishDate {

    Date getFinishDate();

    void getFinishDate(Date startDate);

}
