package ru.tsc.almukhametov.tm.api.service;

public interface ServiceLocator {

    ITaskService getTaskService();

    IProjectService getProjectService();

    IProjectTaskService getProjectTaskService();

    ICommandService getCommandService();

}
